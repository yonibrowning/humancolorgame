﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;

public class GuiEventHandler : MonoBehaviour {

    public void OnFinishButtonPress()
	{
		if (ColorSceneManager._instance) {
			ColorSceneManager currentColorSceneManager = ColorSceneManager._instance;
			currentColorSceneManager.LoadNextScene ();
		} else {
			if (Application.isEditor){
				EditorApplication.ExecuteMenuItem("Edit/Play");
			}
			else {
				Application.Quit();
			}
		}
	}
}
