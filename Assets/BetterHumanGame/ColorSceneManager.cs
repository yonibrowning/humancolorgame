﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.UI;
using UnityEditor;

public class ColorSceneManager : MonoBehaviour {
    public static ColorSceneManager _instance;

	public string patientUnqID;
	public int sceneIndex = 0;

    public List<string> sceneNames = new List<string>();

    public bool justOneScene = false;


   

    // Use this for initialization
    void Start () {

        if (_instance != null)
        {
            Destroy(this.gameObject);
        }

        _instance = this;
		patientUnqID = "Patient Unique ID";
        DontDestroyOnLoad(this.gameObject);

    }

    // Update is called once per frame
    void Update () {
		if (Input.GetKeyDown(KeyCode.Q) && Input.GetKeyDown(KeyCode.LeftControl)|| Input.GetKeyDown(KeyCode.RightControl))
        {
            QuitAndExit();
        }

    }




    public void LoadNextScene()
    {
		if (SceneManager.sceneCountInBuildSettings >= sceneIndex + 1 && !justOneScene) {
			Debug.Log (sceneIndex);
			sceneIndex++;
			Debug.Log (sceneIndex);
			Debug.Log (sceneNames[sceneIndex]);
			LoadSceneByName (sceneNames [sceneIndex]);
		} else {
			//welcome.text = "Thanks for playing!";
			Debug.Log(sceneIndex);
			LoadSceneByName (sceneNames[0]);
		}
    }

	public void LoadSceneByName(string sceneName){
		SceneManager.LoadScene (sceneName);
	}


    public void QuitAndExit()
    {
        if (Application.isEditor){
            EditorApplication.ExecuteMenuItem("Edit/Play");
        }
        else {
            Application.Quit();
        }
    }

    
}
