﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MapInventory : MonoBehaviour, IHasChanged {
    [SerializeField] Transform slots;
    [SerializeField] Text inventoryText;
	
    public void HasChanged()
    {
        inventoryText.fontSize = 14;
        System.Text.StringBuilder builder = new System.Text.StringBuilder();
        builder.Append(" - ");
        foreach(Transform slotTransform in slots)
        {
            GameObject item = slotTransform.GetComponent<Slot>().item;
			if (item) {
				builder.Append (item.name);
				builder.Append ("-");
			} else {
				builder.Append ("null");
				builder.Append ("-");
			}
        }
        inventoryText.text = builder.ToString();

    }
}

namespace UnityEngine.EventSystems
{
    public interface IHasChanged: IEventSystemHandler
    {
        void HasChanged();
    }
}