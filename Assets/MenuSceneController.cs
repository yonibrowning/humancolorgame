﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.UI;
using UnityEditor;

public class MenuSceneController : MonoBehaviour {
	public InputField patientUnqID_field;
	public string chosenScene;
	ColorSceneManager CSM;

	public Button jumpToScene;
	public Button startSequence;

	public GameObject sceneSelectionButtonPrefab;
	public Transform sceneContentParent;

	public Text welcome;


	// Use this for initialization
	void Start () {
		CSM = FindObjectOfType<ColorSceneManager> ();
		patientUnqID_field.text = CSM.patientUnqID;
		LoadSceneLoaderObjects ();
		CSM.sceneNames.Insert (0, SceneManager.GetActiveScene ().name);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void LoadSceneLoaderObjects()
	{
		foreach (string s in CSM.sceneNames)
		{
			Debug.Log (s);
			GameObject go = Instantiate(sceneSelectionButtonPrefab);
			go.transform.SetParent(sceneContentParent);

			Button b = go.GetComponent<Button>();
			AddSceneSelectionListener(b, s);

			jumpToScene.gameObject.SetActive(false);
			patientUnqID_field.onValueChanged.AddListener(delegate { SetPatientUnqID(); });

			go.transform.GetChild(0).GetComponent<Text>().text = s;
		}
	}

	void AddSceneSelectionListener(Button b, string sceneName)
	{
		b.onClick.AddListener(() =>
			{
				this.ChooseScene(sceneName);
			});
	}

	public void ChooseScene(string s)
	{
		chosenScene = s;
		jumpToScene.gameObject.SetActive(true);
	}

	void SetPatientUnqID()
	{
		SetPatientUnqID(patientUnqID_field.text);
	}

	void SetPatientUnqID(string s)
	{
		if (s == string.Empty)
			s = "";
		CSM.patientUnqID = patientUnqID_field.text;
	}

	public void RunJustOneScene()
	{
		CSM.justOneScene = true; 
		//CSM.sceneIndex = SceneManager.sceneCountInBuildSettings;
		CSM.LoadSceneByName(chosenScene);
	}

	public void QuitAndExit(){
		CSM.QuitAndExit ();
	}

	public void LoadNextScene(){
		CSM.LoadNextScene ();
	}

}
